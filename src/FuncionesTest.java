import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FuncionesTest {

    @Test
    void verificarPlacaDigitoRepetido3Veces() {
        String dado = "AAAA123123123123";
        boolean obtengo = Funciones.verificarPlacaDigitoRepetido3Veces(dado);
        assertTrue(obtengo);

        dado = "AAAA";
        obtengo = Funciones.verificarPlacaDigitoRepetido3Veces(dado);
        assertFalse(obtengo);

        dado = "AAAA123123";
        obtengo = Funciones.verificarPlacaDigitoRepetido3Veces(dado);
        assertFalse(obtengo);

        dado = "AAAA123123123";
        obtengo = Funciones.verificarPlacaDigitoRepetido3Veces(dado);
        assertFalse(obtengo);
    }

    @Test
    void has3LetrasConsecutivosTrue() {
        String dado = "123ABC32320";
        boolean obtengo = Funciones.has3LetrasConsecutivos(dado);
        assertTrue(obtengo);
    }

    @Test
    void has3LetrasConsecutivosFalse() {
        String dado = "AB";
        boolean obtengo = Funciones.has3LetrasConsecutivos(dado);
        assertFalse(obtengo);

        dado = "ABB";
        obtengo = Funciones.has3LetrasConsecutivos(dado);
        assertFalse(obtengo);

        dado = "XXY";
        obtengo = Funciones.has3LetrasConsecutivos(dado);
        assertFalse(obtengo);
    }
}