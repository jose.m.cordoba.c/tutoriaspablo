package co.uniquindio.address.model;

public class TipoVehiculo {
    private String nombre;
    private int valorHora;

    public TipoVehiculo(String nombre, int valorHora) {
        this.nombre = nombre;
        this.valorHora = valorHora;
    }


    @Override
    public String toString() {
        return "TipoVehiculo{" +
                "nombre='" + nombre + '\'' +
                ", valorHora=" + valorHora +
                '}';
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getValorHora() {
        return valorHora;
    }

    public void setValorHora(int valorHora) {
        this.valorHora = valorHora;
    }
}
