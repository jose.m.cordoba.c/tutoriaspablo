package co.uniquindio.address.model;

public class Vehiculo {
    private TipoVehiculo tipo;
    private String propietario;
    private String placa;
    private String ciudad;
    private int modelo;

    public Vehiculo(TipoVehiculo tipo, String propietario, String placa, String ciudad, int modelo) {
        this.tipo = tipo;
        this.propietario = propietario;
        this.placa = placa;
        this.ciudad = ciudad;
        this.modelo = modelo;
    }

    @Override
    public String toString() {
        return "Vehiculo{" +
                "tipo=" + tipo +
                ", propietario='" + propietario + '\'' +
                ", placa='" + placa + '\'' +
                ", ciudad='" + ciudad + '\'' +
                ", modelo=" + modelo +
                '}';
    }

    public TipoVehiculo getTipo() {
        return tipo;
    }

    public void setTipo(TipoVehiculo tipo) {
        this.tipo = tipo;
    }

    public String getPropietario() {
        return propietario;
    }

    public void setPropietario(String propietario) {
        this.propietario = propietario;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public int getModelo() {
        return modelo;
    }

    public void setModelo(int modelo) {
        this.modelo = modelo;
    }
}
