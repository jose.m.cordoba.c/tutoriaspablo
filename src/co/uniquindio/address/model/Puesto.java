package co.uniquindio.address.model;

public class Puesto {

    private TipoVehiculo tipo;
    private int numeroReservas;
    private int horas;
    private Vehiculo vehiculo;
    private int ventas;

    public Puesto(TipoVehiculo tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "Puesto{" +
                "tipo=" + tipo +
                ", numeroReservas=" + numeroReservas +
                ", horas=" + horas +
                ", vehiculo=" + vehiculo +
                ", ventas=" + ventas +
                '}';
    }

    public int getVentas() {
        return ventas;
    }
    public boolean isOcupied() {
        return vehiculo != null;
    }

    public TipoVehiculo getTipo() {
        return tipo;
    }

    public void setTipo(TipoVehiculo tipo) {
        this.tipo = tipo;
    }

    public int getNumeroReservas() {
        return numeroReservas;
    }

    public int getHoras() {
        return horas;
    }

    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public int removeVehiculo(){
        int tmp = this.horas * this.tipo.getValorHora();
        this.vehiculo = null;
        ventas += tmp;
        this.horas = 0;
        return tmp;
    }

    public void setVehiculo(Vehiculo vehiculo, int horas) throws Exception {
        if (!isOcupied()) {
            this.vehiculo = vehiculo;
            numeroReservas++;
            this.horas = horas;
        } else {
            throw new Exception("El puesto está ocupado");
        }
    }
}
