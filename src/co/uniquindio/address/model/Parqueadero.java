package co.uniquindio.address.model;

import jdk.jshell.spi.ExecutionControl.NotImplementedException;

import java.util.ArrayList;
import java.util.Arrays;

public class Parqueadero {



    private Puesto puestos[][];
    public static final String[] CIUDADES = {"Bogota", "Armenia", "Pereira", "Manizales", "Cali"};
    public static TipoVehiculo[] TIPOS_VEHICULO = {new TipoVehiculo("Carro", 1000),
                                    new TipoVehiculo("moto", 500)};

    public Parqueadero(int filas, int columnas) {
        puestos = new Puesto[filas][columnas];
        for (int i = 0; i < puestos.length ; i++) {
            for (int j = 0; j < puestos[i].length ; j++) {
                puestos[i][j] = new Puesto(TIPOS_VEHICULO[0]);
            }
        }
    }

    public Puesto[][] getPuestos() {
        return puestos;
    }

    public void imprimirParqueadero(){
        for (Puesto[] fila: puestos) {
            System.out.println(Arrays.toString(fila));
            System.out.println();
        }
    }

    public void imprimirVehiculoPorPoscicion(int fila, int columna){
        System.out.println(puestos[fila][columna].getVehiculo());
    }

    public void insertarVehiculo(Vehiculo v, int horas) throws Exception{
        for (Puesto[] fila: puestos) {
            for (Puesto columna: fila) {
                if (!columna.isOcupied()){
                    columna.setVehiculo(v, horas);
                    return;
                }
            }
        }
        throw new Exception("No hay cupo en el parqueadero");
    }

    public void insertarVehiculo(Vehiculo v, int horas, int fila, int columna) throws Exception{
        if(!puestos[fila][columna].isOcupied()){
            puestos[fila][columna].setVehiculo(v, horas);
        }
        throw new Exception("Ese puesto esta ocupado");
    }

    public int retirarVehiculo(int fila, int columna) throws Exception{
        if(puestos[fila][columna].isOcupied()){
            return puestos[fila][columna].removeVehiculo();
        }
        throw new Exception("No hay vehiculo en la posicion");
    }

    /**
     *
     * Devolver un ArrayList o arreglo con los Puestos que se han reservado
     * más de tres veces en el parqueadero.
     *
     * @return ArrayList<Puesto> Los puestos que han sido reservados mas de 3 veces
     * @throws NotImplementedException
     */
    public ArrayList<Puesto> obtenerPuestosReservadosMasDe3veces() throws NotImplementedException {
        //TODO Hacer esto
        throw new NotImplementedException("No se ha hecho esto");
    }

    /**
     * Devolver un listado ordenado con los nombres de los Propietarios de los vehículos
     * que tienen modelo superior al indicado por el usuario.
     * @param modelo
     * @return
     * @throws NotImplementedException
     */
    public ArrayList<String> propietariosModeloSuperior(int modelo) throws NotImplementedException{
        //TODO Hacer esto
        throw new NotImplementedException("No se ha hecho esto");
    }

    /**
     * Valida si una cadena tiene dos caracteres iguales seguidos
     * @param placa
     * @return
     * @throws NotImplementedException
     */
    public static boolean tieneCaracteresSeguido(String placa) throws NotImplementedException{
        char[] caracteresPlaca = placa.toCharArray();
        int aux = 0;
        for (int i = 1; i < caracteresPlaca.length ; i++) {
            if (caracteresPlaca[aux] == caracteresPlaca[i]) {
                return true;
            }
            aux++;
        }
        return false;
    }

    /**
     * Devolver los propietarios de los vehículos (que están en este momento en el parqueadero) cuyas
     * placas tienen dos vocales seguidas y dos números repetidos.
     * @return
     * @throws NotImplementedException
     */
    public ArrayList<String> propietariosPlacasNumerosSeguidos() throws NotImplementedException{
        //TODO Hacer esto
        ArrayList<String> resultado = new ArrayList<>();
        for (int i = 0; i < puestos.length; i++) {
            for (int j = 0; j < puestos[i].length ; j++) {
                if(tieneCaracteresSeguido(puestos[i][j].getVehiculo().getPlaca())){
                    resultado.add(puestos[i][j].getVehiculo().getPropietario() );
                }
            }
        }
        return resultado;
    }


    /**
     * Agregar un atributo ciudad al vehículo (String) puede inicializarlo de forma aleatoria. Devolver el nombre
     * de la ciudad de la cual hay más vehículos (es decir, tiene placa de esa ciudad).
     * @return
     * @throws NotImplementedException
     */
    public String obtenerCiudadConMasVehiculos() throws NotImplementedException{
        //TODO Hacer esto
        throw new NotImplementedException("No se ha hecho esto");
    }

    /**
     * Eliminar del parqueadero todos los vehículos que tienen placa de una determinada ciudad y modelo entre el
     * rango especificado (pedir los extremos del intervalo para el rango)
     * @param ciudad
     * @param modeloMinimo
     * @param modeloMaximo
     * @throws NotImplementedException
     */
    public void eliminarPorCiudadYModelo(String ciudad, int modeloMinimo, int modeloMaximo)
            throws NotImplementedException{
        //TODO Hacer esto
        throw new NotImplementedException("No se ha hecho esto");
    }

    /**
     * Calcular el total de dinero recaudado en el parqueadero por tipo de puesto.
     * @param tv
     * @return
     * @throws NotImplementedException
     */
    public int calcularDineroPorTipo(TipoVehiculo tv)
            throws NotImplementedException{
        //TODO Hacer esto
        throw new NotImplementedException("No se ha hecho esto");
    }

    /**
     * Informar que categoría ha recaudado más dinero.
     * @return
     * @throws NotImplementedException
     */
    public TipoVehiculo calcularCategoríaConMayorRecaudo()
            throws NotImplementedException{
        //TODO Hacer esto
        throw new NotImplementedException("No se ha hecho esto");
    }
    
}
