package co.uniquindio.address.model;

import jdk.jshell.spi.ExecutionControl;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class ParqueaderoTest {

    @org.junit.jupiter.api.Test
    void tieneCaracteresSeguidosTest() throws ExecutionControl.NotImplementedException {
        String dado = "AAA123";
        boolean resultadoEsperado = true;
        boolean resultadoReal = Parqueadero.tieneCaracteresSeguido(dado);
        assertEquals(resultadoEsperado, resultadoReal);

        dado = "ACB123";
        resultadoEsperado = false;
        resultadoReal = Parqueadero.tieneCaracteresSeguido(dado);
        assertEquals(resultadoEsperado, resultadoReal);

        dado = "ACB133";
        resultadoEsperado = true;
        resultadoReal = Parqueadero.tieneCaracteresSeguido(dado);
        assertEquals(resultadoEsperado, resultadoReal);
    }

    @org.junit.jupiter.api.Test
    void insertarVehiculo() throws Exception {
        TipoVehiculo tv = Parqueadero.TIPOS_VEHICULO[0];
        Parqueadero parqueaderoDePruebas = new Parqueadero(4, 4);
        Vehiculo vehiculo = new Vehiculo(tv,
                "Jose",
                "abc123",
                Parqueadero.CIUDADES[0],
                2020);
        parqueaderoDePruebas.insertarVehiculo(vehiculo, 5);
        parqueaderoDePruebas.imprimirVehiculoPorPoscicion(0,0);
        vehiculo = new Vehiculo(tv,
                "Manuel",
                "xyz321",
                Parqueadero.CIUDADES[2],
                2018);
        parqueaderoDePruebas.insertarVehiculo(vehiculo, 2);
        parqueaderoDePruebas.imprimirVehiculoPorPoscicion(0,1);
    }

    @org.junit.jupiter.api.Test
    void puestosMasDe3VecesTest() throws Exception {
        TipoVehiculo tv = Parqueadero.TIPOS_VEHICULO[0];
        Parqueadero parqueaderoDePruebas = new Parqueadero(4, 4);
        Vehiculo vehiculo = new Vehiculo(tv,
                "Jose",
                "abc123",
                Parqueadero.CIUDADES[0],
                2020);
        parqueaderoDePruebas.insertarVehiculo(vehiculo, 5);
        System.out.println("La cuenta es " + parqueaderoDePruebas.retirarVehiculo(0,0));
        vehiculo = new Vehiculo(tv,
                "Manuel",
                "xyz321",
                Parqueadero.CIUDADES[2],
                2018);
        parqueaderoDePruebas.insertarVehiculo(vehiculo, 2);
        System.out.println("La cuenta es " + parqueaderoDePruebas.retirarVehiculo(0,0));
        vehiculo = new Vehiculo(tv,
                "Manuel",
                "xyz321",
                Parqueadero.CIUDADES[2],
                2018);
        parqueaderoDePruebas.insertarVehiculo(vehiculo, 2);
        System.out.println("La cuenta es " + parqueaderoDePruebas.retirarVehiculo(0,0));
        vehiculo = new Vehiculo(tv,
                "Manuel",
                "xyz321",
                Parqueadero.CIUDADES[2],
                2018);
        parqueaderoDePruebas.insertarVehiculo(vehiculo, 2);
        ArrayList<Puesto> resultadoEsperado = new ArrayList<>();
        resultadoEsperado.add(parqueaderoDePruebas.getPuestos()[0][0]);
        ArrayList<Puesto> resultadoReal = parqueaderoDePruebas.obtenerPuestosReservadosMasDe3veces();
        assertTrue(resultadoReal.equals(resultadoEsperado));
    }

}