import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;

public class Funciones {

    /**
     * verifica si la placa tiene tres dígitos repetidos
     *
     * @param placa
     * @return
     */
    public static boolean verificarPlacaDigitoRepetido3Veces(@NotNull String placa) {
        char[] auxiliar = placa.toCharArray();
        Arrays.sort(auxiliar);
        int poscicion1 = 0, poscicion2 = 1, poscicion3 = 2;
        for (int i = 3; i < auxiliar.length; i++) {
            if (Character.isDigit(auxiliar[poscicion1]) &&
                    auxiliar[poscicion1] == auxiliar[poscicion2] &&
                    auxiliar[poscicion1] == auxiliar[i] &&
                    auxiliar[poscicion1] == auxiliar[poscicion3]) {
                return true;
            }
            poscicion1++;
            poscicion2++;
            poscicion3++;
        }
        return false;
    }

    /**
     * verifica si la placa tiene tres letras consecutivas
     * @param placa
     * @return
     */
    @Contract(pure = true)
    public static boolean has3LetrasConsecutivos(@NotNull String placa) {
        char[] auxiliar = placa.toCharArray();
        int poscicion1 = 0, poscicion2 = 1;
        for (int i = 2; i < auxiliar.length; i++) {
            if(auxiliar[poscicion1] == auxiliar[poscicion2] -1 &&
            auxiliar[poscicion1] == auxiliar[i] - 2){
                return true;
            }
            poscicion1++;
            poscicion2++;
        }
        return false;
    }
}
