Ejercicios de las tutorías de pablo

TALLER DEL PARQUEADERO

1. Construir el diagrama de clases del Parqueadero

![Diagrama de Clases](Diagrama1.png)

2. Devolver un ArrayList o arreglo con los Puestos que se han reservado más de tres veces en el parqueadero.
3. Devolver un listado ordenado con los nombres de los Propietarios de los vehículos que tienen modelo superior al indicado por el usuario.
4. Devolver los propietarios de los vehículos (que están en este momento en el parqueadero) cuyas  placas tienen dos vocales seguidas y dos números repetidos.
5. Agregar un atributo ciudad al vehículo (String) puede inicializarlo de forma aleatoria. Devolver el nombre de la ciudad de la cual hay más vehículos (es decir, tiene placa de esa ciudad).
6. Eliminar del parqueadero todos los vehículos que tienen placa de una determinada ciudad y modelo entre el rango especificado (pedir los extremos del intervalo para el rango)
7. Agregar al registro una cantidad de horas (atributo) y un precio  fijo por hora (dependiendo del tipo). Informar cuanto debe pagar por la reserva realizada.
8. Calcular el total de dinero recaudado en el parqueadero por tipo de puesto. 
9. Informar que categoría ha recaudado más dinero.
